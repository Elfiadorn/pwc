package com.pwc.demo.controller;

import com.google.common.collect.Lists;
import com.pwc.demo.config.DataConfiguration;
import com.pwc.demo.model.Country;
import com.pwc.demo.model.Response;
import com.pwc.demo.util.EdgeConverter;
import com.pwc.demo.util.JourneyMapper;
import lombok.NoArgsConstructor;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@Import(DataConfiguration.class)
@NoArgsConstructor
public class JourneyControllerTest {

    @Autowired
    private Map<String, Country> data;

    @Test
    public void resolveCountries() {
        checkCountries("CZE", "SVK", "ITA");
    }

    private void checkCountries(String... codes) {
        for (String code : codes) {
            Assert.assertNotNull("Country with code " + code + " don't exist", data.get(code));
        }
    }

    @Test
    public void resolveJourney() {
        JourneyMapper journeyMapper = new JourneyMapper(EdgeConverter.convert(data));
        journeyMapper.setSource(data.get("CZE"));

        Response response = new Response(
                journeyMapper.getPath(data.get("ITA"))
                        .stream()
                        .map(Country::getCode)
                        .collect(Collectors.toList())
        );

        Assert.assertNotNull("No route found", response.getRoute());
        Assert.assertEquals(response.getRoute(), Lists.newArrayList("CZE", "AUT", "ITA"));
    }
}