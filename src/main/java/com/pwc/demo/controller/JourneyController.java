package com.pwc.demo.controller;

import com.pwc.demo.model.Country;
import com.pwc.demo.model.Response;
import com.pwc.demo.util.EdgeConverter;
import com.pwc.demo.util.JourneyMapper;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/routing")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class JourneyController {

    private final Map<String, Country> data;
    private JourneyMapper journeyMapper;

    @PostConstruct
    public void init() {
        this.journeyMapper = new JourneyMapper(EdgeConverter.convert(data));
    }

    @GetMapping("/{source}/{target}")
    public Response resolveJourney(@PathVariable("source") String source, @PathVariable("target") String target) {
        log.info("Resolving journey {} - {}", source, target);
        return resolveJourney(data.get(source), data.get(target));
    }

    @NonNull
    private Response resolveJourney(final Country source, final Country target) {
        if (Objects.isNull(source) || Objects.isNull(target)) {
            throw new NullPointerException("Unknown country code.");
        }

        journeyMapper.setSource(source);

        return new Response(
                journeyMapper.getPath(target)
                        .stream()
                        .map(Country::getCode)
                        .collect(Collectors.toList())
        );
    }

    /**
     * Handling situation of non existing country code
     *
     * @return BAD_REQUEST
     */
    @ExceptionHandler({NullPointerException.class})
    public ResponseEntity handleException() {
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

}
