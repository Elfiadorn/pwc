package com.pwc.demo.model;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class Country {

    @SerializedName("cca3")
    private String code;

    private String region;

    private List<Double> latlng;

    private List<String> borders;

}
