package com.pwc.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

@AllArgsConstructor
public class Edge {

    @Getter
    @NonNull
    private final Country source;

    @Getter
    @NonNull
    private final Country destination;

    @Getter
    @NonNull
    private final Double distance;

}
