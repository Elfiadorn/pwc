package com.pwc.demo.config;

import com.pwc.demo.model.Country;
import com.pwc.demo.util.FileReaderUtils;
import com.pwc.demo.util.JsonParserUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.Map;

@Configuration
public class DataConfiguration {

    @Bean
    public Map<String, Country> data() {
        String json = FileReaderUtils.readFile();
        return !StringUtils.isEmpty(json) ?
                JsonParserUtils.parse(json) : Collections.emptyMap();
    }

}
