package com.pwc.demo.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Util for reading json configuration
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileReaderUtils {

    private static final String FILE_NAME = "countries.json";

    @NonNull
    public static String readFile() {
        try {
            InputStream stream = new ClassPathResource(FILE_NAME).getInputStream();
            return IOUtils.toString(stream, StandardCharsets.UTF_8.name());
        } catch (IOException ioe) {
            log.error("Problem with parsing data.", ioe);
            return "";
        }
    }

}
