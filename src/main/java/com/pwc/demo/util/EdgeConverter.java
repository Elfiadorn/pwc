package com.pwc.demo.util;

import com.pwc.demo.model.Country;
import com.pwc.demo.model.Edge;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EdgeConverter {

    @NonNull
    public static List<Edge> convert(@NonNull final Map<String, Country> countryMap) {
        return countryMap.entrySet()
                .stream()
                .flatMap(entry -> createEdges(entry.getValue(), countryMap))
                .collect(Collectors.toList());
    }

    @NonNull
    private static Stream<Edge> createEdges(@NonNull final Country source, @NonNull final Map<String, Country> countryMap) {
        return source.getBorders()
                .stream()
                .map(border -> createEdge(source, countryMap.get(border)));
    }

    @NonNull
    private static Edge createEdge(@NonNull final Country source, @NonNull final Country destination) {
        List<Double> sourceLatlng = source.getLatlng();
        List<Double> destinationLatlng1 = destination.getLatlng();

        double distance = DistanceCalculator.calculateDistance(
                sourceLatlng.get(0),
                sourceLatlng.get(1),
                destinationLatlng1.get(0),
                destinationLatlng1.get(1)
        );

        return new Edge(source, destination, distance);
    }

}
