package com.pwc.demo.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Utils for calculating distances
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class DistanceCalculator {

    /**
     * Calculate distance between two coordinates in km
     */
    static double calculateDistance(double sourceLatitude, double sourceLongtitude, double targetLatitude, double targetLongtitude) {
        double theta = sourceLongtitude - targetLongtitude;
        double dist = Math.sin(deg2rad(sourceLatitude)) * Math.sin(deg2rad(targetLatitude)) +
                Math.cos(deg2rad(sourceLatitude)) * Math.cos(deg2rad(targetLatitude)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return dist * 1.609344;
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

}
