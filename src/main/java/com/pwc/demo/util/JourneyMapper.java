package com.pwc.demo.util;

import com.pwc.demo.model.Country;
import com.pwc.demo.model.Edge;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * JourneyMapper is responsible for getting shortest route between source and target location.
 */
@RequiredArgsConstructor
public class JourneyMapper {

    private final List<Edge> edges;
    private Set<Country> settledNodes;
    private Set<Country> unSettledNodes;
    private Map<Country, Country> predecessors;
    private Map<Country, Double> distance;

    public void setSource(@NonNull final Country source) {
        settledNodes = new HashSet<>();
        unSettledNodes = new HashSet<>();
        distance = new HashMap<>();
        predecessors = new HashMap<>();
        distance.put(source, (double) 0);
        unSettledNodes.add(source);
        while (!unSettledNodes.isEmpty()) {
            Country node = getMinimum(unSettledNodes);
            settledNodes.add(node);
            unSettledNodes.remove(node);
            findMinimalDistances(node);
        }
    }

    /*
     * This method returns the path from the source to the selected target and
     * EMPTY_LIST if no path exists
     */
    @NonNull
    public List<Country> getPath(@NonNull final Country target) {
        LinkedList<Country> path = new LinkedList<>();
        Country step = target;
        // check if a path exists
        if (predecessors.get(step) == null) {
            return Collections.emptyList();
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
    }

    private void findMinimalDistances(@NonNull final Country node) {
        List<Country> adjacentNodes = getNeighbors(node);
        for (Country target : adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node)
                    + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node)
                        + getDistance(node, target));
                predecessors.put(target, node);
                unSettledNodes.add(target);
            }
        }
    }

    @SuppressWarnings("squid:S00112")
    private double getDistance(@NonNull final Country node, @NonNull final Country target) {
        for (Edge edge : edges) {
            if (edge.getSource().equals(node)
                    && edge.getDestination().equals(target)) {
                return edge.getDistance();
            }
        }

        throw new RuntimeException("Should not happen");
    }

    @NonNull
    private List<Country> getNeighbors(@NonNull final Country node) {
        List<Country> neighbors = new ArrayList<>();
        for (Edge edge : edges) {
            if (edge.getSource().equals(node)
                    && !isSettled(edge.getDestination())) {
                neighbors.add(edge.getDestination());
            }
        }
        return neighbors;
    }

    @NonNull
    private Country getMinimum(@NonNull final Set<Country> vertexes) {
        Country minimum = null;
        for (Country vertex : vertexes) {
            if (minimum == null) {
                minimum = vertex;
            } else {
                if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
                    minimum = vertex;
                }
            }
        }

        if (Objects.isNull(minimum)) {
            throw new NullPointerException("Calculating distance with null object");
        }

        return minimum;
    }

    private boolean isSettled(@NonNull final Country vertex) {
        return settledNodes.contains(vertex);
    }

    private double getShortestDistance(@NonNull final Country destination) {
        Double d = distance.get(destination);
        if (d == null) {
            return Double.MAX_VALUE;
        } else {
            return d;
        }
    }

}
