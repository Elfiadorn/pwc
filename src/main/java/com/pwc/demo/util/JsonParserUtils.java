package com.pwc.demo.util;

import com.google.gson.Gson;
import com.pwc.demo.model.Country;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JsonParserUtils {

    @NonNull
    public static Map<String, Country> parse(@NonNull final String json) {
        List<Country> countries = Arrays.asList(new Gson().fromJson(json, Country[].class));
        return countries.stream().collect(Collectors.toMap(Country::getCode, Function.identity()));
    }

}
